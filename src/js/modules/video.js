/* Video Player Modifications */
  createButton = require('./utils').createButton;

  function setupVideoPlayer(){
  	var videoPlayer = videojs.getPlayers()[Object.keys(videojs.getPlayers())[1]];
  	var path = window.location.pathname;
  	var lastPosition = localStorage.getItem(path);
  	var currentPosition = 0;
    videoPlayer.on('loadedmetadata', function(){
      initVideoUI(videoPlayer)
    })
		//initVideoUI(videoPlayer);

		videoPlayer.on('play', function(){
  		if(lastPosition && lastPosition > currentPosition){
  				videoPlayer.currentTime(lastPosition);
  		}
  		setInterval(function(){
  				var duration = videoPlayer.duration();
  				currentPosition = videoPlayer.currentTime();
  				if(currentPosition + 20 > duration){
  						currentPosition = 0;
  				}
  				localStorage.setItem(path, currentPosition);
  		},3000);
		});
  }

  function initVideoUI(videoPlayer){
  	var rewind = createButton("rewind", "ion-ios-rewind", "Skip Backward");
  	var fastforward = createButton("fastforward", "ion-ios-fastforward", "Skip Forward");
    var fullWindow = createButton("fullwindow", "ion-arrow-expand", "Full Window");

  	rewind.on("click", function(){
  		videoPlayer.currentTime(videoPlayer.currentTime() - 10);
  	});

  	fastforward.on("click", function(){
  		videoPlayer.currentTime(videoPlayer.currentTime() + 30);
  	});
    fullWindow.on("click", function(){
      videoEl = $("#video .video-js");
      buttonIcon = $(this).find('.icon');
      if(videoEl.hasClass("fullwindow-video")){
        buttonIcon.removeClass('ion-arrow-shrink').addClass('ion-arrow-expand');
        $('body').removeClass('no-overflow');
        videoEl.removeClass("fullwindow-video");
      } else {
        buttonIcon.removeClass('ion-arrow-expand').addClass('ion-arrow-shrink');
        $('body').addClass('no-overflow');
        videoEl.addClass("fullwindow-video");
      };
    });

  	var customBar = $("<span>");
  	customBar.append(rewind);
  	customBar.append(fastforward);
  	$('.vjs-control-bar .vjs-play-control').after(customBar);
    $('.vjs-control-bar .vjs-fullscreen-control').after(fullWindow);
  }

module.exports = setupVideoPlayer;
