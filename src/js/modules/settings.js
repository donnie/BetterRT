var SettingsContent = ' \
<li> \
  <input type="radio" name="tabs" id="tab-betterrt-settings"> \
  <label for="tab-betterrt-settings" class="tab-betterrt-settings"><h2>BetterRT</h2></label> \
  <div id="tab-content-betterrt-settings" class="tab-content"> \
    <h2 class="content-title">BetterRT Settings</h2> \
    <label for="preferred_quality">Preferred Quality</label> \
    <select id="betterrt_preferred_quality" name="preferred_quality"> \
      <option value="240p">240p</option> \
      <option value="360p">360p</option> \
      <option value="480p">480p</option> \
      <option value="720p">720p</option> \
      <option value="1080p">1080p</option> \
      <option value="auto">auto</option> \
    </select> \
  </div> \
</li> \
';

function CreateSettingsPage(){
      $("#body-block ul.tabs").append(SettingsContent);
      $('#betterrt_preferred_quality').on('change', function(){
        localStorage.setItem('preferred_quality', $(this).val())
      })
}

module.exports = CreateSettingsPage;
