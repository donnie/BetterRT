function createButton(id, iconClass, title){
  title = title || '';
  var button = $("<button>");
  button.attr('id', id);
  button.addClass("betterRT");
  button.css({paddingTop:"5px"});
  button.attr('Title', title)

  var icon = $("<i>");
  icon.addClass("icon");
  icon.addClass(iconClass);
  icon.css({fontSize: "18px"});
  button.append(icon);
  return button;
};

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length,c.length);
        }
    }
    return null;
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

module.exports = {
  createButton: createButton
  , getCookie: getCookie
  , setCookie: setCookie
}
