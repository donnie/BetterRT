/* Comments Modifications */
  function liveCommentEdit(){
  	$('#comments').on('click', 'a.edit-button', function(e){
  		e.preventDefault();
      var editButton = this
  		var url = $(this).attr('href');
      var commentId = $(editButton).parent().parent().attr('id')
  		$.get(url, function(body){
  			 var editHTML = $(body).find('#'+commentId).html();
  			 $('#'+commentId).html(editHTML).addClass('live-comment-editor');
         var editor = $('.live-comment-editor');

         RT.redactor(editor.find('.redactor'))

         var form = $('.live-comment-editor form');
         form.submit(function(e) {

             var url = form.attr('action');


             $.ajax({
                    type: "POST",
                    url: url,
                    data: form.serialize()
                  }).done(function(){
                    refreshComments();
                  });

             e.preventDefault(); // avoid to execute the actual submit of the form.
         });

  		});
  	});
  }

  function refreshCommentsIcon(){
  	$('#comments').find('.content-title').append('<a id="refresh-comments">&nbsp;<i class="fa fa-refresh"></i></a>');
  	$("#refresh-comments").on('click', function(){
      $('#refresh-comments .fa').addClass('spin-custom');
  		 refreshComments();
  	});
  }

  function refreshComments(){
    console.log('refreshing comments')
  	$.get(window.location.href, function(body){
  			$("#comments").html($(body).find("#comments").html());
  			$('#comments').find('.content-title').append('<a id="refresh-comments">&nbsp;<i class="fa fa-refresh"></i></a>');
        RT.redactor($('.comment-input .redactor'))
        liveCommentEdit();
  	});
  }

module.exports = {
  liveEdit: liveCommentEdit
  , refresh: refreshCommentsIcon
}
