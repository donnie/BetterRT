
/* Recently Added Modifications */
  var lsdb = require('../vendor/localStorageDB');

  var DB = new lsdb("db", localStorage);
  if(!DB.tableExists("videos")){
    DB.createTable("videos", ["title", "channel", "posted", "image", "link", "length", "postedString"]);
  }


  function AddRecentlyAddedBar(channel) {
    checkLastUpdated()

    var SelectorTemplate = require('../../templates/recent/selector.ejs');
    $("#body-block .row").prepend(SelectorTemplate());
    for(ch in channelHash){
      if(channelHash[ch].indexOf(window.location.host)+1 && channelHash[ch].indexOf(window.location.host) < 9){
        channel = ch
      }
    }
    $('#recently-added-channel-select').val(channel)
    $('#recently-added-channel-select').change(function(){
        changeChannelTo($(this).val())
    })
    $("#refresh_videos").on('click', function(){
      $('#refresh_videos .fa').addClass('spin-custom');
      updateVideoDB(null, function(){
        $('#refresh_videos .fa').removeClass('spin-custom');
      })
    })
  };


  var channelHash = {
    "rooster_teeth": "http://roosterteeth.com/",
    "achievement_hunter": "http://achievementhunter.roosterteeth.com/",
    "funhaus": "http://funhaus.roosterteeth.com/",
    "screwattack": "http://screwattack.roosterteeth.com/",
    "game_attack": "http://gameattack.roosterteeth.com/",
    "the_know": "http://theknow.roosterteeth.com/",
    "cow_chop": "http://cowchop.roosterteeth.com/"
  }

  function changeChannelTo(channel){
    $('.grid-blocks').empty();
    blockTemplate = require('../../templates/recent/videoBlock.ejs');
    var queryOptions = {
      limit: 24,
      sort: [["posted", "DESC"]]
    };
    if(channel != 'all') {
      queryOptions.query = {
          channel: channel
        }
    }
    Videos = DB.queryAll("videos", queryOptions);
    if(Videos.length){
      Videos.forEach(function(video){
        $('.grid-blocks').append(blockTemplate(video));
      })
    } else {
      updateVideoDB(null, function(){
        changeChannelTo($('#recently-added-channel-select').val());
      })
    }
  }

  var channelsDone = 0;
  var totalChannels = Object.keys(channelHash).length
  function updateVideoDB(channels, callback){
    if(!channels){
      channelsDone = 0;
    }
    channels = channels || Object.keys(channelHash)
    channel = channels.pop()
    console.log('Updating '+channel);
    updateChannel(channel, function(){
      channelsDone++;
      if(channelsDone == totalChannels){
        console.log("Finished updating videos.")
        require('./utils').setCookie('brt_recent_last_updated', (new Date()).valueOf(), 365);
        callback()
      }
    })
    if(channels.length){
      updateVideoDB(channels, callback);
    }
  }

  function updateChannel(channel, callback){
    $.get(channelHash[channel]+'/episode/recently-added', function(body){
      $(body).find('.grid-blocks li').each(function(){
        var videoInfo = {
          title: $(this).find('.name').text(),
          channel: channel,
          postedString: $(this).find('.post-stamp').text(),
          posted: new Date($(this).find('.post-stamp').text()).valueOf(),
          image: $(this).find('.image-container img').attr('src'),
          link: $(this).find('a').attr('href'),
          length: $(this).find('.timestamp').text()
        };
        DB.insertOrUpdate("videos", {"link": videoInfo.link}, videoInfo);
      })
      DB.commit();
      callback()
    })
  }

  function checkLastUpdated() {
    now = new Date().valueOf();
    lastUpdated = require('./utils').getCookie('brt_recent_last_updated') || 0;
    if(lastUpdated + (1000 * 60 * 60) < now){
      updateVideoDB()
    }
  }

module.exports = AddRecentlyAddedBar
