$(document).ready(function(){
  $ = jQuery;

  if(document.getElementById("video")){
    Video = require('./modules/video');
    Video()
    Comments = require('./modules/comments')
    Comments.liveEdit();
    Comments.refresh();
  };

  if(window.location.pathname == "/episode/recently-added"){
    require('./modules/recent')()
  }

  if(window.location.pathname == "/my/settings"){
    require('./modules/settings')()
  }
});
