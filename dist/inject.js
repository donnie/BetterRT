chrome.extension.sendMessage({}, function(response) {
	var readyStateCheckInterval = setInterval(function() {
		if (document.readyState === "interactive"){//"interactive") {
			clearInterval(readyStateCheckInterval);

			// ----------------------------------------------------------
			// Inject the main script into the page.
			var script = document.createElement("script");
			script.defer = true;
			script.src = chrome.extension.getURL("plugin.js");
			document.body.appendChild(script);
			// ----------------------------------------------------------


		}
	}, 10);
});
