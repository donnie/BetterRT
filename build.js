var fs = require('fs-extra');
var archiver = require('archiver');
var browserify = require('browserify');
var b = browserify();
b.add('./src/js/main.js');

b.transform(require("browserify-ejs"));

b.transform({
  global: true
}, 'uglifyify');

JSWriter = fs.createWriteStream('./dist/plugin.js')

JSWriter.on('close', function(){
  fs.copySync('./src/js/inject.js', './dist/inject.js');
  fs.copySync('./src/icons/', './dist/icons');
  fs.copySync('./src/css/plugin.css', './dist/css/plugin.css');
  fs.copySync('./manifest.json', './dist/manifest.json');

  var PluginOutput = fs.createWriteStream('./BetterRT.zip');
  var PluginArchiver = archiver('zip');

  PluginOutput.on('close', function () {
      console.log(PluginArchiver.pointer() + ' total bytes');
      console.log('Built Release ZIP');
  });

  PluginArchiver.on('error', function(err){
    throw err;
  });

  PluginArchiver.pipe(PluginOutput);
  PluginArchiver.bulk([
      { expand: true, cwd: './dist', src: ['**'], dest: './'}
  ]);
});

JSWriter.on('error',function(err){
  throw err;
})


b.bundle().on('error', function(err){console.log(err)}).pipe(JSWriter)
